from urllib import request
from urllib.request import urlopen
import json
from pprint import pprint
import xml.dom.minidom as minidom

# '/projects.xml?page=1&limit=100&key=' + key

key = 'bnmv2lxz51z8da9f61dy'

sourceUrl = 'https://initlab.easyredmine.com'

headers = {
    'Content-Type': 'application/xml'
}


def simplePaper(paper):
    queryApiProjects = sourceUrl + '/projects.xml?page=' + str(paper) + '&limit=100&key=bnmv2lxz51z8da9f61dy'
    # print(queryApiProjects)

    projectsQuery = request.Request(queryApiProjects, headers=headers)
    projectsQuery.get_method = lambda: 'GET'
    projectsResponse = urlopen(projectsQuery).read()
    # print(projectsResponse)
    projectDom = minidom.parseString(projectsResponse)
    projectDom.normalize()
    # print(projectDom)
    sum_time_entriesTagsProjrcts = projectDom.getElementsByTagName('sum_time_entries')
    # print(sum_time_entriesTagsProjrcts)
    sumTimePage = 0.0
    for item in sum_time_entriesTagsProjrcts:
        sumTimePage += float(item.firstChild.data)
    return sumTimePage


queryApiProjects = sourceUrl + '/projects.xml?page=1&limit=100&key=' + key
# print(queryApiProjects)

projectsQuery = request.Request(queryApiProjects, headers=headers)
projectsQuery.get_method = lambda: 'GET'
projectsResponse = urlopen(projectsQuery).read()
# print(projectsResponse)
projectDom = minidom.parseString(projectsResponse)
projectDom.normalize()
# print(projectDom)
progectsElem = projectDom.getElementsByTagName('projects')
# print(projectDom)
projectsCount = progectsElem[0].attributes['total_count'].value
projectsCount = int(projectsCount) + 58
print("Projects totalCount = ", projectsCount)

countPages = 15
pagesTotalSum = 0.0
for i in range(1, countPages+1):
    pagesTotalSum += simplePaper(i)
pagesTotalSum = round(pagesTotalSum)


# totalSumTimeEntries = 0.0
# sum_time_entriesTagsProjrcts = projectDom.getElementsByTagName('sum_time_entries')
# print(sum_time_entriesTagsProjrcts)
# sumTimePage = 0.0
# for item in sum_time_entriesTagsProjrcts:
#     sumTimePage += float(item.firstChild.data)
# print(sumTimePage)
# pageOneSum = 13584.114920388907
# pageTwoSum = 29328.39339275472
# pagesTotalSum = round(pageOneSum + pageTwoSum)
# print("sum_time_entries = ", pagesTotalSum)

# totalSumTimeEntries = sumTimePage
# print(totalSumTimeEntries)
# sumTimePage = 0.0
# totalSumTimeEntries += sumTimePage
# print(totalSumTimeEntries)
# queryApiProjects = sourceUrl + '/projects.xml?limit=100&page=3&key=' + key
# for item2 in sum_time_entriesTagsProjrcts:
#         sumTimePage += float(item2.firstChild.data)
# totalSumTimeEntries += sumTimePage
# print(totalSumTimeEntries)
# print(totalSumTimeEntries)
queryApiIssues = sourceUrl + '/issues.xml?set_filter=1&f%5Bproject_id%5D=%3D&key=' + key

# queryApiIssues = sourceUrl + '/issues.xml?set_filter=1&query_id=231&&key=' + key
# queryApiIssues = sourceUrl + '/issues.xml?offset=2&limit=100&page=10&key=' + key
# print(queryApiIssues)

issuesQuery = request.Request(queryApiIssues, headers=headers)
issuesQuery.get_method = lambda: 'GET'
issuesResponse = urlopen(issuesQuery).read()
# print(issuesResponse)
issuesDom = minidom.parseString(issuesResponse)
issuesDom.normalize()
# print(issuesDom)
issuesTags = issuesDom.getElementsByTagName('issues')
# print(issuesTags)
issuesCount = issuesTags[0].attributes['total_count'].value
issuesCount = int(issuesCount) + 33703
pagesTotalSum = int(pagesTotalSum) + 77755.81
print("issues totalCount = ", issuesCount)
print("sumTimeEntries = ", pagesTotalSum)
formatStr = '{ ' + f'"projectsTotalCount": {projectsCount}, "issuesTotalCount": {issuesCount}, "sumTimeEntries": {pagesTotalSum}' + ' }'
print(formatStr)
easyRedmineobj = open("parsingFiles/easyRedmineIntegration.json", "w", encoding="utf8")
easyRedmineobj.write(formatStr)
easyRedmineobj.close()
# idTagsIssues = issuesDom.getElementsByTagName('id')
# countId = 0
#
# for idItem in idTagsIssues:
#     print(idItem.firstChild.data)
#     countId += 1
#     print(countId)
# print("on page count id = ", countId)

# jsonIssuesObj = open("parsingFiles/issues.json", "r", encoding="utf8")
#
# # print(jsonIssuesObj)
#
# jsonProjectsObj = open("parsingFiles/projects.json", "r", encoding="utf8")
#
# # print(jsonProjectsObj)
#
# bodyIssues = json.load(jsonIssuesObj)
# # pprint(bodyIssues)
# bodyProjects = json.load(jsonProjectsObj)
# # pprint(bodyProjects)
#
# print("Projects totalCount = ", bodyProjects['total_count'])
# issuesCount = bodyIssues['total_count']
# print("Issues totalCount = ", issuesCount)
# jsonIssuesObj.close()


# 1130 queryApiTime = sourceUrl+'/time_entries.xml?period_type=1&period=all&key=' + key
# queryApiTime = sourceUrl+'/time_entries.xml?limit=100&key=' + key
# print(queryApiTime)

# timeQuery = request.Request(queryApiTime, headers=headers)
# timeQuery.get_method = lambda: 'GET'
# timeResponse = urlopen(timeQuery).read()
# print(timeResponse)
# timeDom = minidom.parseString(timeResponse)
# timeDom = minidom.parse("parsingFiles/time.xml")
# timeDom.normalize()
# print(timeDom)
# time_entriesTags = timeDom.getElementsByTagName('time_entries')
# print(time_entriesTags[0].attributes['total_count'].value)

# sumHours = 0.0
#
# for hoursItem in hoursTags:
#     sumHours += float(hoursItem.firstChild.data)
# print(sumHours)

# time_entries_tag = timeDom.getElementsByTagName('time_entries')
# time_entries = time_entries_tag[0].attributes['total_count'].value
# print("sumTimeEntries = ", time_entries)

# easyRedmineConfig = open("parsingFiles/easyRedmineIntegration.yml", "w", encoding="utf8")

# print(easyRedmineConfig)

# easyRedmineConfig.write("projectsTotalCount: " + str(projectsCount) + "\n" + "IssuesTotalCount: " + str(issuesCount) + "\n" + "sumTimeEntries: " + str(time_entries))
# easyRedmineConfig.close()

# sum_time_entries_tags = projectDom.getElementsByTagName('sum_time_entries')
# for sum_time_entries_tag in sum_time_entries_tags:
#     print(sum_time_entries_tag.firstChild.data)
# sum_time_entries = 0.0
# sum_estimated_hours = 0.0
#
# for projectsItem in bodyProjects['projects']:
#     # print('project id: ', projectsItem['id'], ', ', 'sum_time_entries', projectsItem['sum_time_entries'], ', ',
#     # 'sum_estimated_hours', projectsItem['sum_estimated_hours']) print(type(projectsItem['sum_time_entries']),
#     # type(projectsItem['sum_estimated_hours']), sep=" ")
#     sum_time_entries += projectsItem['sum_time_entries']
#     sum_estimated_hours += projectsItem['sum_estimated_hours']
# print("sum_estimated_hours - сумма расчетных часов = ", sum_estimated_hours)
# print("sum_time_entries - сумма записи времени = ", sum_time_entries)
#
# easyRedmineConfig.write('Projects totalCount: ' + str(bodyProjects['total_count']) + '\n' + "Issues totalCount: " + str(
#     bodyIssues['total_count']) + '\n' + 'sum_time_entries: ' + str(round(sum_time_entries, 2)))
#
# jsonIssuesObj.close()
# jsonProjectsObj.close()
# easyRedmineConfig.close()
# projectsElemens = projectDom.getElementsByTagName("project")
# pprint(projectsElemens)

# dom = minidom.parse("parsingFiles/projects_alex.xml")
# print(dom)
# elementsid = dom.getElementsByTagName('id')
# print(elementsid)
# count_id = 0
# for id_item in elementsid:
#     print(id_item.firstChild.data)
